const mongoose = require('mongoose')

const Schema = mongoose.Schema
//Schema lay xai structure banuxa documents haru ko
const workoutSchema = new Schema(
    {
        title: {
            type: String,
            required: true
        },
        load: {
            type: String,
            required: true
        },
        reps: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
)

//model haru lay xai kind of table jasto act garxa ani its called collection in database
module.exports = mongoose.model('Workout', workoutSchema)
