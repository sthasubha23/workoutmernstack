require('dotenv').config()
const express = require('express')
const workoutRoutes = require('./routes/workout')
const cors = require('cors')

const mongoose = require('mongoose')

//express app
const app = express()


// converts fronteend data into
app.use(express.json())



const allowedOrigins = ['http://localhost:3000'];

app.use(
  cors({
    origin: (origin, callback) => {
      // Check if the request origin is allowed
      if (!origin || allowedOrigins.includes(origin)) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
  })
);

//routes
app.use('/api/workout', workoutRoutes)


//middleware
app.use((req, res, next) => {
    console.log(req.path, req.method)
    next()
})

//connect db
mongoose.connect(process.env.MONG_URI)
    .then(() => {

        // listen for request
        app.listen(process.env.PORT, () => {
            console.log(`Connect to db  listen on ${process.env.PORT}`)
        })

    })
    .catch((error) => {
        console.log(error) 
    })
