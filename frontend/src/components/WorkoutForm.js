import React, {  useState } from 'react'
import { useWorkoutsContext } from "../hooks/useWorkoutContext"

const WorkoutForm = () => {
    const { dispatch } = useWorkoutsContext()
    const [title, setTitle] = useState("")
    const [load, setLoad] = useState("")
    const [reps, setReps] = useState("")
    const [error, setError] = useState(null)
    const [emptyFields, setEmptyFields] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault()

        const workout = { title, load, reps }

        const response = await fetch('http://localhost:8080/api/workout', {
            method: 'POST',
            body: JSON.stringify(workout),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const json = await response.json()

        if (!response.ok) {
            setError(json.error)
            setEmptyFields(json.emptyFields)
        }
        if (response.ok) {
            setTitle('')
            setLoad('')
            setReps('')
            setError(null)
            setEmptyFields([])
            console.log('newWorkout', json);
            dispatch({ type: 'CREATE_WORKOUT', payload: json })
        }
    }


    return (
        <form className='create' onSubmit={handleSubmit}>
            <h3>Add a new Workout</h3>

            <label>Exercise title:</label>
            <input
                className={emptyFields.includes('title') ? 'error' : ''}
                type="text"
                onChange={(e) => setTitle(e.target.value)}
                value={title}
            />

            <label>Load:</label>
            <input
                className={emptyFields.includes('load') ? 'error' : ''}
                type="text"
                onChange={(e) => setLoad(e.target.value)}
                value={load}
            />

            <label>Reps:</label>
            <input
                className={emptyFields.includes('reps') ? 'error' : ''}
                type="text"
                onChange={(e) => setReps(e.target.value)}
                value={reps}
            />
            <button>Submit</button>
            {error && <div className='error'>{error}</div>}
        </form>
    )
}

export default WorkoutForm